import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss'],
})
export class ChartsComponent implements OnInit {
  content: any;
  @Input() data: any;

  chartOptions: any;
  constructor() { }

  ngOnInit() {

    this.setData();
    //console.log(this.data, this.content);

  }

  setData() {
    let labels = new Array();
    this.data.values.forEach((el: any) => {
      let index = labels.findIndex((ele) => ele.data == el.data);
      index >= 0
        ? labels[index].count++
        : labels.push({ data: el.data, count: 1 });
    });

    this.content = {
      labels: labels.map((el) => el.data),
      datasets: [
        {
          label: this.data.data_title,
          backgroundColor: [
            '#EC407A',
            '#AB47BC',
            '#42A5F5',
            '#7E57C2',
            '#66BB6A',
            '#FFCA28',
            '#26A69A'
          ],
          borderColor: '#26A69A',
          /*  backgroundColor: 'rgba(179,181,198,0.2)',
           pointBackgroundColor: '#66BB6A',
           pointBorderColor: '#fff',
           pointHoverBackgroundColor: '#fff',
           pointHoverBorderColor: 'rgba(179,181,198,1)', */
          data: labels.map((el) => {
            return el.count;
          }),
        },
      ],
    };

  }
}
