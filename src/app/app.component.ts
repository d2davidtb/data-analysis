import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MenuItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { count } from 'rxjs';
import { PopupComponent } from './popup/popup.component';
import { ApiAnalisisService } from './services/api-analisis.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [DialogService],
})
export class AppComponent implements OnInit {
  public format: any;
  public data: any = [];
  public loading: boolean = true;
  public chartTypes: any = [];
  public tabSelected: any;
  display: boolean = false;
  public counter = 0;
  constructor(private apiService: ApiAnalisisService, public dialogService: DialogService, private dialog: MatDialog) { }

  ngOnInit() {
    this.apiService.getData().subscribe(data => {
      this.data = data
      this.apiService.getFormat().subscribe(f => {
        this.format = f
        this.fixFormat(this.format)
      })
      this.loading = false
    })
    
    this.chartTypes = this.apiService.getChartTypes()
  }

  fixFormat(formats: any) {
    var allFormats = []
    for (let f of formats) {
      var newFormat = Object.assign({}, f)
      newFormat.graphs = this.adaptGraphData(f)
      allFormats.push(newFormat)
    }
    this.format = allFormats
  }

  open(data: any) {
    if (data.popup) {
      return this.openPopup(data)
    } else {
      this.tabSelected = data
    }   
  }

  openPopup(data: any) {
    data.display = true
  }

  adaptGraphData(rule: any) {
    var graphs = []
    for (const element of rule.graphs) {
      var valuesContent: any[] = []
      for (let value of this.data) {
        valuesContent.push(
          {
            id: this.getRecursive(value, element.id_location.split(".")),
            data: this.getRecursive(value, element.data_location.split("."))
          }
        )
      }
      var graphData = {
        data_title: element.data_title,
        id_title: this.getRecursive(this.data[0], element.id_title.split("."), element.id_title),
        chart_type: this.chartTypes[element.type],
        values: valuesContent,
        data_resolution: element.data_resolution,
        graph_space: element.graph_space,
        rounded: element.rounded
      }
      graphs.push(graphData)
    }

    return graphs
  }

  getRecursive(obj: any, location: any[], default_value: any = null): any {
    var c = 0
    var r = null;
    for (let loc of location) {
      c++
      r = obj[loc]
      if (typeof r == "object") {
        r = this.getRecursive(r, location.slice(c), default_value)
      }
      if (typeof r == "undefined") {
        return default_value
      } else {
        return r
      }
    }
  }
}
