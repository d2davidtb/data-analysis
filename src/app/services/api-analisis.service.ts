import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiAnalisisService {
  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get('http://g301-1.fernandoyepesc.com/api.php').pipe(map((data: any) => {

      let value = data.filter(((el:any) => el.valores.length>0))
      
      console.log(value);
      return value
    }))
  }
  getFormat() {
    return this.http.get('http://g301-1.fernandoyepesc.com/format.php').pipe(map((data: any) => {
      let dataR = JSON.parse(data)
      dataR.forEach((element: any) => {
        element.rows = Array(element.rows).fill(1).map((x, i) => i);
        element.cols = Array(element.cols).fill(1).map((x, i) => i);
      });
      return dataR
    }))
  }
  getChartTypes() {
    return {
      "1": "radar",
      "2": "bar",
      "3": "line",
      "4": "doughnut",
      "5": "polarArea"
    }
  }


}
