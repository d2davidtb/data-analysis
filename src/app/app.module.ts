import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { ChartModule } from 'primeng/chart';
import { TableComponent } from './table/table.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsComponent } from './charts/charts.component';
import { PopupComponent } from './popup/popup.component';
import {DialogModule} from 'primeng/dialog';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {MatDialogModule} from '@angular/material/dialog';
@NgModule({
  declarations: [
    AppComponent,
    ChartsComponent,
    TableComponent,
    PopupComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ToolbarModule,
    ButtonModule,
    HttpClientModule,
    TableModule,
    SplitButtonModule,
    ChartModule,
    SplitButtonModule,
    FlexLayoutModule,
    DialogModule,
    DynamicDialogModule,
    DragDropModule,
    MatDialogModule
  ],
  exports: [
    TableComponent,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
